from __future__ import print_function

import os.path
import pickle

from google.auth.transport.requests import Request
from google.oauth2 import service_account
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload

# If modifying these scopes, delete the file token.pickle_bkp.
SCOPES = ['https://www.googleapis.com/auth/drive']
CREDS_FILE = "creds.json"
FOLDER_ID = "1VDuqte30QiMT_dI5uXSeacg3AeV0Slyx"
DRIVE_ACC_CRED = "DRIVE_ACC_CRED"


def main():
    """Shows basic usage of the Drive v3 API.
    Prints the names and ids of the first 10 files the user has access to.
    """

    file = open(CREDS_FILE, 'w+')
    file.write(os.getenv(DRIVE_ACC_CRED))
    file.close()

    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            creds = service_account \
                .Credentials \
                .from_service_account_file(CREDS_FILE, scopes=SCOPES)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('drive', 'v3', credentials=creds)

    # Call the Drive v3 API
    results = service.files().list(
        pageSize=10, fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])

    if not items:
        print('No files found.')
    else:
        for item in items:
            if item['name'].endswith(".pdf"):
                service.files().delete(fileId=item.get('id')).execute()

    # Upload all pdf from the drive folder
    for filename in os.listdir("drive"):
        filepath = "drive/" + filename
        if filename.endswith(".pdf"):
            file_metadata = {
                'name': filename,
                'parents': [FOLDER_ID]
            }
            media = MediaFileUpload(filepath,
                                    mimetype='application/pdf')
            upload_file = service.files().create(body=file_metadata,
                                                 media_body=media,
                                                 fields='id').execute()
            print('File (ID: %s) has been uploaded' % upload_file.get('id'))


if __name__ == '__main__':
    drive_service = init_drive_service()
    cleanup_drive(drive_service)
    upload_files_from_folder(drive_service, FOLDER_ID, "drive/")

