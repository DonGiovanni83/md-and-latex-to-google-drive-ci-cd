#!/bin/bash
if [ ! -d "drive" ]; then
    mkdir drive
fi
for filename in tex/*.tex; do
        pandoc "$filename" -o "drive/$(basename "$filename" .tex).pdf"
done
